import Vuex from 'vuex'

import * as actions from './actions'
import * as mutations from './mutations'
import * as getters from './getters'

const createStore = () => {
  return new Vuex.Store({
    state: {
      url: 'https://rickandmortyapi.com/api/character/?',
      characters: [],
      prevPage: null,
      nextPage: null
    },
    mutations,
    actions,
    getters
  })
}

export default createStore
