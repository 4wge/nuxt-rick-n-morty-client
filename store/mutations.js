export const characters = (state, characters) => {
  state.characters = characters
}
export const prevPage = (state, url) => {
  state.prevPage = url
}
export const nextPage = (state, url) => {
  state.nextPage = url
}
