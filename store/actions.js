import axios from 'axios'

export const nuxtServerInit = (vuexContext, context) => {
  const url = context.store.state.url
  return axios.get(url)
    .then((res) => {
      vuexContext.commit('prevPage', res.data.info.prev)
      vuexContext.commit('nextPage', res.data.info.next)
      vuexContext.commit('characters', res.data.results)
    })
    .catch((error) => {
      console.log(error)
    })
}

export const getCharacters = ({ commit, state }, args) => {
  let url = state.url
  if (args && args.url) {
    url = args.url.concat('&')
  }
  console.log('url for axios is', url)
  axios.get(url)
    .then((res) => {
      commit('prevPage', res.data.info.prev)
      commit('nextPage', res.data.info.next)
      commit('characters', res.data.results)
    })
    .catch((error) => {
      console.log(error)
    })
}
