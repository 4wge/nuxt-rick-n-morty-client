export const characters = (state) => {
  return state.characters
}
export const previousPage = (state) => {
  return state.prevPage
}
export const nextPage = (state) => {
  return state.nextPage
}
